import { Component } from '@angular/core';
import { IngredientService } from './services/ingredient/ingredient.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'recipeprojectclient';

  constructor() {}

}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { baseUrl } from '../../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: Http) { }

  public get(endpoint: string): Observable<any> {
    return this.http.get(baseUrl + endpoint);
  }

  public post(endpoint: string, body: any): Observable<any> {
    return this.http.post(baseUrl + endpoint, body);
  }
}

import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor(private _httpService: HttpService) { }

  public getRecipe(startOfName) {
    return this._httpService.get('recipes' + '/' + startOfName);
  }

  public getRecipeById(id) {
    return this._httpService.get('recipes/rec' + '/' + id);
  }

  public getRecipeByCategory(category) {
    return this._httpService.get('recipes/category' + '/' + category);
  }

  public postRecipe(body) {
      return this._httpService.post('recipes', body);
  }
}

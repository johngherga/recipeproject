import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  constructor(private _httpService: HttpService) { }

  public getIngredient(startOfName) {
    return this._httpService.get('ingredients' + '/' + startOfName);
  }
}

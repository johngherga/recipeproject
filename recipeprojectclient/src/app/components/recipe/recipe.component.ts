import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../../services/recipe/recipe';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {

  recipes = [];
  nutrients = [];
  displayNutrients = [];
  oldServings: number;
  oldIngredientAmount: number;
  newIngredientAmount: number;
  filteredNutrients = {
    Protein: 0,
    Kolhydrater: 0,
    Energi: 0,
    Fett: 0,
    Mfet: 0,
    Ofet: 0,
    Salt: 0
  };
  servings = [ 2 , 4 , 6 , 8 ];

  constructor(private router: ActivatedRoute, private _recService: RecipeService, private route: Router) {}

  ngOnInit() {
    this.getRecipes();
  }

  getRecipes() {
    this.router.params.subscribe(params => {
      this._recService.getRecipeById(params.id)
      .subscribe(res => {
        this.recipes.push(res.json());
        this.getNutrients();
      });
   });
   console.log('Logging recipes' , this.recipes);
  }

  // Getting the nutrients for the ingredients in this recipe
  // Only protein, kolhydrater, energi and converting the original value
  // of 100 grams to whatever the admin put in as grams when added the recipe
  // then divided by 4 so the nutrients in a recipe will be per person
  getNutrients() {
   this.recipes[0].Ingredients.forEach(ingredient => {
      ingredient.Naringsvarden.forEach(nutrient => {
        if (nutrient.Namn === 'Protein') {
          this.filteredNutrients.Protein += (ingredient.ViktGram / 100) * (nutrient.Varde) / 4;
          this.filteredNutrients.Protein = Math.round(this.filteredNutrients.Protein);
        }
        if (nutrient.Namn === 'Kolhydrater') {
          this.filteredNutrients.Kolhydrater += (ingredient.ViktGram / 100) * (nutrient.Varde) / 4;
          this.filteredNutrients.Kolhydrater = Math.round(this.filteredNutrients.Kolhydrater);
        }
        if (nutrient.Forkortning === 'Ener') {
          this.filteredNutrients.Energi += (ingredient.ViktGram / 100) * (nutrient.Varde) / 4;
          this.filteredNutrients.Energi = Math.round(this.filteredNutrients.Energi);
        }
        if (nutrient.Namn === 'Fett') {
          this.filteredNutrients.Fett += (ingredient.ViktGram / 100) * (nutrient.Varde) / 4;
          this.filteredNutrients.Fett = Math.round(this.filteredNutrients.Fett);
        }
        if (nutrient.Namn === 'Salt') {
          this.filteredNutrients.Salt += (ingredient.ViktGram / 100) * (nutrient.Varde) / 4;
          this.filteredNutrients.Salt = Math.round(this.filteredNutrients.Salt);
        }
        if (nutrient.Forkortning === 'Mfet') {
          this.filteredNutrients.Mfet += (ingredient.ViktGram / 100) * (nutrient.Varde) / 4;
          this.filteredNutrients.Mfet = Math.round(this.filteredNutrients.Mfet);
        }
        if (nutrient.Forkortning === 'Ofet') {
          this.filteredNutrients.Ofet += (ingredient.ViktGram / 100) * (nutrient.Varde) / 4;
          this.filteredNutrients.Ofet = Math.round(this.filteredNutrients.Ofet);
        }
      });
   });
  this.displayNutrients.push(this.filteredNutrients);
  }

  // Getting and keeping track of the old servings number when changing servings
  // in the select dropdown
  getOldServings(oldServings) {
    this.oldServings = oldServings;
    console.log(this.oldServings);
  }

  // Updating the amount of each ingredient based on how many servings is chosen
  updateIngredients(newServings) {
    this.recipes.map(res => {
      res.Ingredients.forEach(ingredient => {
        this.oldIngredientAmount = ingredient.Amount;
          this.newIngredientAmount = (this.oldIngredientAmount * newServings) / this.oldServings;
          ingredient.Amount = this.newIngredientAmount;
      });
    });
  }

  homePage() {
    this.route.navigate(['']);
  }
}

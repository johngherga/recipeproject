import { Component, OnInit } from '@angular/core';
import { IngredientService } from '../../services/ingredient/ingredient.service';
import { RecipeService } from '../../services/recipe/recipe';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  myControl = new FormControl();
  homePageCategory: any[] = ['Bakverk', 'Italienskt', 'Indiskt', 'Vegetariskt'];
  recipes: any;
  constructor(public _recService: RecipeService, public router: Router) {}

  ngOnInit() {}

  onKey($event) {
    const name = $event.target.value;
    if (name.length > 1) {
      this._recService.getRecipe(name)
      .subscribe(res => {
        this.recipes = res.json();
      });
    } else {
      console.log('at least 2 characters');
    }
  }

  homePage() {
    this.router.navigate(['']);
  }

  loadRecipePage(recipe) {
    this.router.navigate(['recipe/' + recipe._id]);
  }

  loadCategoryPage(category) {
    this.router.navigate(['recipe/category/' + category]);
  }
}

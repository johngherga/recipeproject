import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../../services/recipe/recipe';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  recipes = [];
  title = { Name: '' };
  constructor(private router: ActivatedRoute, private _recService: RecipeService, private route: Router) { }

  ngOnInit() {
    this.getRecipes();
  }

  getRecipes() {
    this.router.params.subscribe(params => {
      this.title.Name = params.category;
      this._recService.getRecipeByCategory(params.category)
      .subscribe(res => {
        this.recipes.push(res.json());
      });
   });
  }

  navigateToRecipe(id) {
    this.route.navigate(['recipe/' + id]);
  }

  homePage() {
    this.route.navigate(['']);
  }
}

import { Component, OnInit } from '@angular/core';
import { IngredientService } from '../../services/ingredient/ingredient.service';
import { FormControl } from '@angular/forms';
import { RecipeService } from '../../services/recipe/recipe';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  myControl = new FormControl();
  ingredients: any[] = [];
  displayIngredients: any[] = [];
  selectedIngredient: any;
  recipeName: any;
  recipeCategory: any;
  recipeDescription: any;
  recipeImgUrl: any;
  unitInGrams: number;
  constructor(public _ingService: IngredientService, public _recService: RecipeService) {}

  ngOnInit() {}

  onKey($event) {
    const name = $event.target.value;
    if (name.length > 1) {
      this._ingService.getIngredient(name)
      .subscribe(res => {
        this.ingredients = res.json();
      });
    } else {
      console.log('at least 2 characters');
    }
  }

  onSelected(ingredient) {
    console.log(ingredient);
    this.selectedIngredient = ingredient;
  }

  addIngredient(measurement, amount, unitInGrams) {
    this.selectedIngredient.Measurement = measurement;
    this.selectedIngredient.Amount = parseFloat(amount);
    this.selectedIngredient.ViktGram = parseFloat(unitInGrams);
    this.displayIngredients.push(this.selectedIngredient);
  }

  submitRecipe(recipeName, recipeCategory, recipeImgUrl, recipeDescription) {
    const body = {
    Name: recipeName,
    Category: recipeCategory,
    Servings: 4,
    Instructions: recipeDescription,
    Ingredients: this.displayIngredients,
    imgURL: recipeImgUrl
    };

    this._recService.postRecipe(body)
      .subscribe();
      alert('Ditt recept har lagts till i databasen');
  }
}

import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { AdminComponent } from './components/admin/admin.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import { CategoryComponent } from './components/category/category.component';


const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'admin', component: AdminComponent },
    { path: 'recipe/:id', component: RecipeComponent},
    { path: 'recipe/category/:category', component: CategoryComponent}
  ];

  @NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
  })
  export class AppRoute {}

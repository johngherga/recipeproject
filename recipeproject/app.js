const express = require('express');
const app = express();
const mongoose = require('mongoose');
const mongoDB = 'mongodb://localhost:27017/RecipeProject';
const ingredientRoutes = require('./api/routes/ingredients');
const recipeRoutes = require('./api/routes/recipes');
const cors = require('cors');
const bodyParser = require('body-parser');

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Connected to mongoDB');
});

mongoose.connect(mongoDB, { useNewUrlParser: true });

app.use(cors());
app.use(bodyParser());
app.use(express.static('www'));
app.use('/ingredients', ingredientRoutes);
app.use('/recipes', recipeRoutes);

module.exports = app;
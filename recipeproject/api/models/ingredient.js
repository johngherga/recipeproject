const mongoose = require('mongoose');

const ingredientSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    Nummer: String,
    Namn: String,
    ViktGram: Number,
    Huvudgrupp: String,
    Naringsvarden: {}
});

module.exports = mongoose.model('Ingredient', ingredientSchema);
const mongoose = require('mongoose');

const recipeSchema = mongoose.Schema({
    Name: String,
    Category: String,
    Servings: Number,
    Instructions: String,
    Ingredients: [],
    imgURL: String
});

module.exports = mongoose.model('Recipe', recipeSchema);
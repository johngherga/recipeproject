const express = require('express');
const router = express.Router();
const Ingredient = require('../models/ingredient');
const mongoose = require('mongoose');
let ldata = require('../../json/livsmedelsdata');

// GET ingredients by category from DB
router.get('/category/:ingredientCategory' , (req, res, next) => {
    const category = req.params.ingredientCategory;
    Ingredient.find({Huvudgrupp: category})
    .exec()
    .then(docs => {
        res.status(200).json(docs);
    })
    .catch(err => {
        res.status(500).json({
            error: err
        });
    });
});

// GET ingredients by AUTO COMPLETE
router.get('/:startOfName', (req, res, next) => {
    const id = req.params.startOfName;
    if (id.length < 2) {
        res.send({
            message: 'Enter at least 2 characters'
        });
    }
    Ingredient.find({Namn: new RegExp('^' + id , 'i' )})
    .exec()
    .then(doc => {
        res.status(200).json(doc);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
});

router.post('/', (req, res, next) => {
    res.status(200).json({
        message: 'Handling POST requests to /ingredients'
    });
});

// Add all ingredients from the json file livsmedelsdata to the DB
// DO NOT TRY TO DO THIS AGAIN
router.post('/addalltodb', (req, res, next) => {
    ldata.forEach(item => {
        const ingredient = new Ingredient({
            _id: new mongoose.Types.ObjectId(),
            Nummer: item.Nummer,
            Namn: item.Namn,
            ViktGram: item.ViktGram,
            Huvudgrupp: item.Huvudgrupp,
            Naringsvarden: []
        });
        item.Naringsvarden.Naringsvarde.forEach(item => {
            ingredient.Naringsvarden.push({
                Namn: item.Namn ? item.Namn : undefined,
                Forkortning: item.Forkortning ? item.Forkortning : undefined,
                Varde: item.Varde ? parseFloat(item.Varde) : undefined,
                Enhet: item.Enhet ? item.Enhet : undefined,
                Ursprung: item.Ursprung ? item.Ursprung : undefined,
                Publikation: item.Publikation ? item.Publikation : undefined,
                Framtagningsmetod: item.Framtagningsmetod ? item.Framtagningsmetod : undefined,
                Referenstyp: item.Referenstyp ? item.Referenstyp : undefined
            });
        });
        ingredient.markModified([
            'Nummer',
            'Namn',
            'ViktGram',
            'Huvudgrupp',
            'Naringsvarden'
        ]);
        ingredient.save();
    });
    res.status(200).json({
        message: 'Handling POST requests to /ingredients/addalltodb'
    });
});

module.exports = router;
const express = require('express');
const router = express.Router();
const Recipe = require('../models/recipe');
const mongoose = require('mongoose');


// GET recipes by NAME AUTO COMPLETE 
router.get('/:startOfName', (req, res, next) => {
    const name = req.params.startOfName;
    if (name.length < 2) {
        res.send({
            message: 'Enter at least 2 characters'
        });
    }
    Recipe.find({Name: new RegExp('^' + name , 'i' )})
    .exec()
    .then(doc => {
        res.status(200).json(doc);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
});

// GET recipes by Category
router.get('/category/:category' , (req, res, next) => {
    const CATEGORY = req.params.category;
    Recipe.find({Category: CATEGORY})
    .exec()
    .then(doc => {
        res.status(200).json(doc);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
});

// GET recipes by ID
router.get('/rec/:id', (req, res, next) => {
    const id = req.params.id;
    Recipe.findById(id)
    .exec()
    .then(doc => {
        res.status(200).json(doc);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error: err});
    });
});

// POST to recipes in DB
router.post('/', (req, res, next) => {
    if(!req.body) {
        res.send('Error: No Body');
    }
    const recipe = new Recipe(req.body);
    recipe.save()
    .then(docs => { 
        res.status(200).json(docs)
    })
    .catch(err => console.log(err));
});

module.exports = router;